Team:
Yuxan Cui, ycui15@jhu.edu
Shaokai Lin, slin63@jhu.edu

Design:
The overall structure is distributed in three main parts. Under the main function is the initiation of the program (one that takes in
value for whether to load a game or start a new one). In Board.cpp there is the next layer of our program, which has its main body
under ChessGame.run. This is the main logical loop function for the game and takes in all the required commands from the user, such
as saving, board, and forfeit functions. The third part of our program is located under Chess.cpp, marked as ChessGame::makeMove,
this is where a move of the piece is executed and tested for validity. Functions such as the promotion of pawns, castling, and Check
handling are all located here.

Completeness:
Castling function is fairly complete, but does not check for whether King passes through a checked position on its way.

Special instructions:
There are no special instructions
