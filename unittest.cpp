#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <numeric>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;


int main() {
    int count=0;
    string s = PieceSymbol(0);
    if(s=="♙") {
        cout<<"PASSED PieceSymbol"<<endl;
        count++;
    }

    string move="A1A2";

    vector<Position> pos;
    pos=user_move(move);
    if(pos[0].x==0 && pos[0].y==7 && pos[1].x==0 && pos[1].y==6) {
        cout<<"PASSED user_move"<<endl;
        count++;
    }

    char a=indextoalpha(0);
    if(a=='a') {
        cout<<"PASSED indextoalpha"<<endl;
        count++;
    }

    int b=alphatoindex('a');
    if(b==0) {
        cout<<"PASSED alphatoindex"<<endl;
        count++;
    }

    ChessGame chess;
    int valid;

    chess.initPiece(PAWN_ENUM, WHITE, Position(4,6));
    Piece *tmp1=chess.getPiece(Position(4,6));
    valid = tmp1->validMove(Position(4,6), Position(4,7), chess);
    if (valid==1) {
        cout<<"PASSED pawn move"<<endl;
        count++;
    }

    chess.initPiece(ROOK_ENUM, WHITE, Position(5,1));
    Piece *tmp2=chess.getPiece(Position(5,1));
    valid=tmp2->validMove(Position(5,1), Position(5,5), chess);
    if (valid==1) {
        cout<<"PASSED rook move"<<endl;
        count++;
    }

    chess.initPiece(KNIGHT_ENUM, WHITE, Position(6,1));
    Piece *tmp3=chess.getPiece(Position(6,1));
    valid=tmp3->validMove(Position(6,1), Position(7,3), chess);
    if (valid==1) {
        cout<<"PASSED knight move"<<endl;
        count++;
    }

    chess.initPiece(KING_ENUM, WHITE, Position(7,1));
    Piece *tmp4=chess.getPiece(Position(7,1));
    valid=tmp4->validMove(Position(7,1), Position(7,0), chess);
    if (valid==1) {
        cout<<"PASSED king move"<<endl;
        count++;
    }

    chess.initPiece(QUEEN_ENUM, WHITE, Position(3,1));
    Piece *tmp5=chess.getPiece(Position(3,1));
    valid=tmp5->validMove(Position(3,1), Position(1,3), chess);
    if (valid==1) {
        cout<<"PASSED queen move"<<endl;
        count++;
    }

    chess.initPiece(BISHOP_ENUM, WHITE, Position(0,0));
    Piece *tmp6=chess.getPiece(Position(0,0));
    valid=tmp6->validMove(Position(0,0), Position(3,3), chess);
    if (valid==1) {
        cout<<"PASSED bishop move"<<endl;
        count++;
    }

    int turn=1;
    int move_result = chess.makeMove(Position(4,6), Position(4,7), turn);
    if (move_result==1) {
        cout<<"PASSED makeMove"<<endl;
        count++;
    }

    Piece *promote=chess.getPiece(Position(4,7));
    if(promote->id()==4) {
        cout<<"PASSED pawn romotion"<<endl;
        count++;
    }
    chess.current_board(true);


    if (count==12) {
        cout<<"PASSED ALL TESTS"<<endl;
    }
    else {
        cout<<"FAILED "<<12-count<<" test(s)"<<endl;
    }

    return 0;
}