#include <assert.h>
#include <cctype>
#include <iostream>
#include <algorithm>
#include <fstream>

#include "Game.h"
#include "Prompts.h"
#include "Chess.h"
#include "Terminal.h"
#include "Prompts.h"

using namespace std;

///////////////
// Board //
///////////////

Board::~Board() {
    // Delete all pointer-based resources
    for (unsigned int i=0; i < m_width * m_height; i++)
        delete m_pieces[i];
    for (size_t i=0; i < m_registeredFactories.size(); i++)
        delete m_registeredFactories[i];
}

// Get the Piece at a specific Position, or nullptr if there is no
// Piece there or if out of bounds.
Piece* Board::getPiece(Position position) const {
    if (validPosition(position)) {
        return m_pieces[index(position)];
    }
    else {
        Prompts::outOfBounds();
        return nullptr;
    }
}

// Create a piece on the board using the factory.
// Returns true if the piece was successfully placed on the board
bool Board::initPiece(int id, Player owner, Position position) {
    Piece* piece = newPiece(id, owner);
    if (!piece) return false;

    // Fail if the position is out of bounds
    if (!validPosition(position)) {
        Prompts::outOfBounds();
        return false;
    }
    // Fail if the position is occupied
    if (getPiece(position)) {
        Prompts::blocked();
        return false;
    }
    m_pieces[index(position)] = piece;
    return true;
}

// Add a factory to the Board to enable producing
// a certain type of piece
bool Board::addFactory(AbstractPieceFactory* pGen) {
    // Temporary piece to get the ID
    Piece* p = pGen->newPiece(WHITE);
    int id = p->id();
    delete p;

    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        m_registeredFactories[id] = pGen;
        return true;
    } else {
        std::cout << "Id " << id << " already has a generator\n";
        return false;
    }
}

// Search the factories to find a factory that can translate `id' to
// a Piece, and use it to create the Piece. Returns nullptr if not found.
Piece* Board::newPiece(int id, Player owner) {
    PieceGenMap::iterator it = m_registeredFactories.find(id);
    if (it == m_registeredFactories.end()) { // not found
        std::cout << "Id " << id << " has no generator\n";
        return nullptr;
    } else {
        return it->second->newPiece(owner);
    }
}

string PieceSymbol(int id) {
    if(id==0) {
        return "\u2659";
    }
    if(id==1) {
        return "\u2656";
    }
    if(id==2) {
        return "\u2658";
    }
    if(id==3) {
        return "\u2657";
    }
    if(id==4) {
        return "\u2655";
    }
    if(id==5) {
        return "\u2654";
    }
    return "\0";
}

void ChessGame::current_board(bool display_board) {
    if(display_board==true) {
        cout<<"    a  b  c  d  e  f  g  h"<<endl;
        Terminal T;
        for(int i=0; i<8; i++) {
            for(int k=0; k<3; k++) {
                if (k==1) {
                    cout<<" "<<8-i<<" ";
                }
                else {
                    cout<<"   ";
                }
                for(int j=0; j<8; j++) {
                    if( (i%2!=0 && j%2!=0)||(i%2==0 && j%2==0) ) {
                        T.colorAll(false,Terminal::RED,Terminal::GREEN);
                    }
                    else {
                        T.colorAll(false,Terminal::RED,Terminal::RED);
                    }
                    if(k==1) {
                        Piece* tmp=getPiece(Position{(unsigned int)j, (unsigned int)i});
                        cout<<" ";
                        if (tmp!=nullptr) {
                            if(tmp->owner()==0) {
                                T.colorFg(true,Terminal::WHITE);
                            }
                            if(tmp->owner()==1) {
                                T.colorFg(true,Terminal::BLACK);
                            }
                            cout<<PieceSymbol(tmp->id());
                        }
                        else {
                            cout<<" ";
                        }
                        cout<<" ";
                    }
                    else {
                        cout<<"   ";
                    }
                    T.set_default();
                }
                if (k==1) {
                    cout<<" "<<8-i<<" ";
                }
                printf("\n");
            }
        }
        cout<<"    a  b  c  d  e  f  g  h"<<endl;
    }
}

void ChessGame::turn(int m_turn) {
    if(m_turn%2==0) {
        cout<<"Black turn ";
    }
    if(m_turn%2!=0) {
        cout<<"White turn ";
    }
    cout<<m_turn<<":";
}


vector<Position> user_move(string move) {
    vector<Position> pos;
    if((move[0]>='A' && move[0]<='H')&&
            (move[2]>='A' && move[2]<='H')&&
            move[1]-'0'<=8 && move[1]-'0'>=1&&
            move[3]-'0'<=8 && move[3]-'0'>=1 && move.size()==4) {
        unsigned int x1= int(move[0])-64;
        unsigned int x2= int(move[2])-64;
        unsigned int y1= move[1]-'0';
        unsigned int y2= move[3]-'0';
        pos.push_back(Position());
        pos[0].x=x1-1;
        pos[0].y=8-y1;
        pos.push_back(Position());
        pos[1].x=x2-1;
        pos[1].y=8-y2;
        // cout<<pos[0].x<<" "<<pos[0].y<<" "<<pos[1].x<<" "<<pos[1].y<<endl;
        return pos;
    }
    else {
        Prompts::outOfBounds();
    }
    return pos;
}

char indextoalpha(int column) {
    if(column==0) {
        return 'a';
    }
    if(column==1) {
        return 'b';
    }
    if(column==2) {
        return 'c';
    }
    if(column==3) {
        return 'd';
    }
    if(column==4) {
        return 'e';
    }
    if(column==5) {
        return 'f';
    }
    if(column==6) {
        return 'g';
    }
    if(column==7) {
        return 'h';
    }
    return '\0';
}

int alphatoindex(char column) {
    if(column=='a') {
        return 0;
    }
    if(column=='b') {
        return 1;
    }
    if(column=='c') {
        return 2;
    }
    if(column=='d') {
        return 3;
    }
    if(column=='e') {
        return 4;
    }
    if(column=='f') {
        return 5;
    }
    if(column=='g') {
        return 6;
    }
    if(column=='h') {
        return 7;
    }
    return -1;
}

void ChessGame::saveGame() {
    ofstream fp;
    string input;
    Prompts::saveGame();
    getline(cin,input);
    fp.open(input, ios::out);
    fp<<"chess\n"<<m_turn<<endl;
    for (int i; i<(int)m_pieces.size(); i++) {
        Piece *tmp=m_pieces[i];
        if (tmp!=nullptr) {
            int row = (int)(i)/8;
            int column =(i)%8;
            char a=indextoalpha(column);
            fp<<tmp->owner()<<" ";
            fp<<a;
            fp<<(8-row)<<" "<<tmp->id()<<endl;
        }
    }
    fp.close();
}

void ChessGame::loadGame() {
    ifstream fp;
    int success=0;
    while (success==0) {
        Prompts::loadGame();
        string name;
        string line;
        getline(cin,name);

        fp.open(name, ios::in);
        getline(fp,line);

        if (fp.is_open()) {
            getline(fp,line);
            int turn=stoi(line);
            m_turn=turn;
            while(getline(fp,line)) {
                initPiece((PieceEnum)(line[5]-'0'),(Player)(line[0]-'0'),Position((alphatoindex(line[2])),(8-(line[3]-'0'))));
            }
            success=1;
        }
        else {
            Prompts::loadFailure();
        }

    }
    fp.close();
}

void user_input(string &move) {
    getline(cin,move);               //take the entire user input as a single string
    for (int i=0; i<(int)move.size(); i++) { //change string to upper letter, then remove all spaces
        move.at(i)=toupper(move.at(i));
    }
    move.erase(remove_if(move.begin(), move.end(), ::isspace), move.end());
}

void ChessGame::check_castle(Position start, Piece * tmp) {
    if (tmp->id()==5) {
        if(tmp->owner()==0) {
            castle.at(0)=false;
            castle.at(1)=false;
        }
        if(tmp->owner()==1) {
            castle.at(2)=false;
            castle.at(3)=false;
        }
    }
    if (tmp->id()==1) {
        if ( start.x==0 && start.y==7 ) {
            castle.at(2)=false;
        }
        if ( start.x==7 && start.y==7 ) {
            castle.at(3)=false;
        }
        if ( start.x==0 && start.y==0 ) {
            castle.at(0)=false;
        }
        if ( start.x==7 && start.y==0 ) {
            castle.at(1)=false;
        }
    }
}

int ChessGame::execute_castle(Position start, Position end,Piece * tmp) {
    if( (start.x==4 && start.y==0) || (start.x==4 && start.y==7) ) {
        //by this point we know that the move is a castling move
        vector<Position> movelist {Position(2,0),Position(6,0),Position(2,7),Position(6,7)};
        vector<Position> rooklist {Position(0,0),Position(7,0),Position(0,7),Position(7,7)};

        int diff= ((int)end.x-(int)start.x)/2;																	//make sure no piece is in the way
        int x=start.x+diff;

        while (x!=(int)end.x) {
            cout<<x<<endl;
            if (getPiece(Position(x,start.y))!=nullptr) {
                Prompts::blocked();
                return -1;
            }
            /////////////////////////////
            ////Jerry Please add HERE////
            /////////////////////////////

            x+=diff;
        }
        int ind;                  //find the index for which of the four types of castling
        for (vector<Position>::iterator it=movelist.begin(); it!=movelist.end(); ++it) {
            if (end.x==it->x && end.y==it->y) {
                ind=distance(movelist.begin(),it);
                break;
            }
        }

        if (castle.at(ind)==true) {
            Piece * rook= getPiece(rooklist.at(ind));
            m_pieces[index(start)]=nullptr;
            m_pieces[index(end)]=tmp;
            m_pieces[index(Position(end.x-diff,end.y))]=rook;
            m_pieces[index(rooklist.at(ind))]=nullptr;
        }
        else {
            Prompts::cantCastle();
            return -3;
        }
    }
    else {
        Prompts::illegalMove();
        return -2;
    }
    return 1;
}


void ChessGame::run() {               //main logical loop for chess game
    string move;
    bool display_board=false;
    turn(m_turn);

    while(1) {
        vector<Position> pos;
        user_input(move);
        if(move.compare("BOARD")==0) {
            display_board= !display_board;
            current_board(display_board);
        }
        else if(move.compare("FORFEIT")==0) {
            if (m_turn>1) {
                int trn=Board::turn();
                Player pl= (Player)(m_turn % 2);
                Prompts::win(pl, trn-1);
                Prompts::gameOver();
                cout<<endl;
                break;
            }
            else {
                cout<<"Make a move before forfeit"<<endl;
            }
        }
        else if(move.compare("SAVE")==0) {
            saveGame();
            cout<<"Game Saved"<<endl;
        }
        else if(move.compare("Q")==0) {
            break;
        }
        else if (isalpha(move[0]) && isalpha(move[2]) && isdigit(move[1]) && isdigit(move[3])) {
            pos=user_move(move);
            if (pos.size()==2) {
                int moveStatus = makeMove(pos[0],pos[1],m_turn);
                if(moveStatus!=0) {
                    current_board(display_board);
                    if (moveStatus == 3) {
                        int trn=Board::turn();
                        Player pl= (Player)(m_turn % 2);
                        Prompts::win(pl, trn-1);
                        Prompts::gameOver();
                        cout<<endl;
                        break;
                    }
                    else if (moveStatus == 4) {
                        Prompts::staleMate();
                        Prompts::gameOver();
                        cout<<endl;
                        break;
                    }
                }
            }
        }
        else {
            Prompts::parseError();
        }
        turn(m_turn);
        cin.clear();
    }
    // delete &m_registeredFactories;
}



