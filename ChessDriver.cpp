#undef FOR_RELEASE

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <numeric>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;



int main() {
    string input;
    Prompts::menu();
    cin>>input;
    ChessGame chess;
    while(input.compare("1")!=0 && input.compare("2")!=0) {
        Prompts::menu();
        cin>>input;
    }
    cin.clear();
    cin.ignore();
    if(input.compare("1")==0) {
        //start game
        chess.setupBoard();
        chess.run();
    }
    else if(input.compare("2")==0) {
        chess.loadGame();
        chess.run();
    }
    return 0;
}
