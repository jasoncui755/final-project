#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <algorithm>
#include <numeric>
#include "Game.h"
#include "Chess.h"
#include "Prompts.h"
#include "Terminal.h"
using namespace std;

void ChessGame::checkPromotion() {
    for (int i = 0; i < 8; i++) {
        if (m_pieces[index(Position(i,0))] != nullptr && m_pieces[index(Position(i,0))]->id() == PAWN_ENUM) {
            m_pieces[index(Position(i,0))] = newPiece(QUEEN_ENUM,m_pieces[index(Position(i,0))]->owner());
        }
        if (m_pieces[index(Position(i,7))] != nullptr && m_pieces[index(Position(i,7))]->id() == PAWN_ENUM) {
            m_pieces[index(Position(i,7))] = newPiece(QUEEN_ENUM,m_pieces[index(Position(i,7))]->owner());
        }
    }
}

bool ChessGame::checkValidThreat(Position p) {
    Piece *enemy = getPiece(p);
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (getPiece(Position(i,j)) != nullptr && getPiece(Position(i,j))->owner() != enemy->owner() && getPiece(Position(i,j))->validMove(Position(i,j), p, *(this)) == 1) {
                return false;
            }
        }
    }
    return true;
}

int ChessGame::checkStalemate(Player pl) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Piece *it = getPiece(Position(i,j));
            if (it != nullptr && it->owner()==pl) {
                for (int x = 0; x < 8; x++) {
                    for (int y = 0; y < 8; y++) {
                        if (it->validMove(Position(i,j), Position(x,y), *(this)) == 1) {
                            return 0;
                        }
                    }
                }
            }
        }
    }
    return 1; //not stalemate
}

int ChessGame::checkIntercept(Position threatPos, Position preyPos) {
    Piece *prey = getPiece(preyPos);
    int diff_x = 0, diff_y = 0;
    int dx=preyPos.x-threatPos.x;
    int dy=preyPos.y-threatPos.y;
    if (dx > 0) {
        diff_x=1;
    }
    else if (dx < 0) {
        diff_x=-1;
    }
    if (dy < 0) {
        diff_y=-1;
    }
    else if (dy > 0) {
        diff_y=1;
    }
    unsigned int x=threatPos.x, y=threatPos.y;
    while (x!=(preyPos.x) || y!=(preyPos.y)) {
        x+=diff_x;
        y+=diff_y;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece *it = getPiece(Position(i,j));
                if (it != nullptr && it->id() != KING_ENUM && it->owner()==prey->owner()) {
                    if (it->validMove(Position(i,j), Position(x,y), *(this)) == 1) {
                        return 0;
                    }
                }
            }
        }
    }
    return 1;
}

int ChessGame::checkCheckMate(Piece *king) {
    //1.check if any piece can eliminate threat
    //2.if not, compute king moves in total
    //3.go through these moves to see if king's position can be reached by any opposing piece
    //1.run checkCheck for each piece and see if a piece can get to King
    //2.if anything can move in the way and eliminate threat, we are good
    //3.if a piece can eat king and the path is clear, add the position into trapList

    //if cant eliminate threat and trapList size == moveList's size, King has nowhere to go => checkMate
    std::vector<Piece*> threatList;
    std::vector<Position> trapList;
    std::vector<Position> moveList = king->getKingMoves(KingPos(king->owner()), *(this));
    for (std::vector<Position>::iterator it = moveList.begin(); it != moveList.end(); ++it) {
        for (unsigned int i = 0; i < 8; i++) {
            for (unsigned int j = 0; j < 8; j++) {
                // if (m_pieces[index(Position(i,j))] != nullptr && m_pieces[index(Position(i,j))]->owner() == king->owner() && m_pieces[index(Position(i,j))]->validMove(Position(i,j), ))
                if (m_pieces[index(Position(i,j))] != nullptr && m_pieces[index(Position(i,j))]->owner() != king->owner()) {
                    int valid = m_pieces[index(Position(i,j))]->validMove(Position(i,j), *it, *(this));
                    //check whether Rook, Queen, Bishop can be blocked.
                    if (valid == 1 && (getPiece(Position(i,j))->id()==ROOK_ENUM || getPiece(Position(i,j))->id()==BISHOP_ENUM || getPiece(Position(i,j))->id()==QUEEN_ENUM)) {
                        valid = checkIntercept(Position(i,j), KingPos((Player)(!getPiece(Position(i,j))->owner())));
                    }
                    if (valid == 1) {
                        bool validThreat = checkValidThreat(Position(i,j));
                        bool found = false;
                        for (std::vector<Position>::iterator k = trapList.begin(); k != trapList.end(); ++k) {
                            if (i == k->x && j == k->y) {
                                found = true;
                            }
                        }
                        if (!found && validThreat) {
                            trapList.push_back(Position(i,j));
                        }
                    }
                }
            }
        }
    }
    if (trapList.size() == moveList.size()) return 1;
    else return 0;
}

//locate player's king
Position ChessGame::KingPos(Player pl) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (m_pieces[index(Position(i,j))] != nullptr && m_pieces[index(Position(i,j))]->id() == KING_ENUM && m_pieces[index(Position(i,j))]->owner() == pl) {
                return Position(i,j);
            }
        }
    }
    return Position(8,8); //else return a fake position
}

//return 1 if checked, 0 if not, 2 if checkMate
int ChessGame::checkCheck(Position start, Piece * it, Position kingPos) {
    //find the enemy king
    if (m_pieces[index(kingPos)] != nullptr && m_pieces[index(kingPos)]->id() == KING_ENUM && m_pieces[index(kingPos)]->owner() != it->owner()) {
        int valid = it->validMove(start, kingPos, *(this));
        if (valid == 1) {
            Piece * king = m_pieces[index(kingPos)];
            if (checkCheckMate(king)) {
                Prompts::checkMate(it->owner());
                return 2;
            }
            Prompts::check(it->owner());
            return 1;
        }
        else {
            return 0;
        }
    }
    return 0;
}

int ChessGame::surveyCheck(Player pl) {   //see if player pl is checked on current board
    Position kp=KingPos(pl);
    for (int i=0; i<8; i++) {
        for(int j=0; j<8; j++) {
            Piece *tmp=getPiece(Position(i,j));
            if (tmp!=nullptr) {
                if(tmp->owner()==m_turn%2) {    //not pl's piece
                    if (tmp->validMove(Position(i,j),kp,*(this))==1) {
                        return 1;
                    }
                }
            }
        }
    }
    return 0;
}



// Make a move on the board. Return an int, with < 0 being failure
int ChessGame::makeMove(Position start, Position end, int &m_turn) {
    Piece* tmp=getPiece(start);
    if(tmp!=nullptr) {
        if (m_turn%2!=tmp->owner()) {
            int valid = tmp->validMove(start, end, *(this));

            if (valid == 1) {
                check_castle(start,tmp);
                Piece * capture=m_pieces[index(end)];
                if (capture!=nullptr) {
                    Prompts::capture(tmp->owner());
                }

                int see_check=surveyCheck(tmp->owner());
                int numofcheck=0;
                if (see_check==1) {                //see if piece is checked before movement
                    numofcheck++;
                }

                m_pieces[index(end)] = tmp;          //make movement
                m_pieces[index(start)] = nullptr;

                see_check=surveyCheck(tmp->owner());
                if (see_check==1) {             //see if piece is checked after movement
                    numofcheck++;
                    m_pieces[index(start)]=tmp;
                    m_pieces[index(end)]=nullptr;
                }
                else {
                    numofcheck = 0;
                }

                if (numofcheck==1) {
                    Prompts::cantExposeCheck();
                    return 0;
                }
                else if (numofcheck==2) {
                    Prompts::mustHandleCheck();
                    return 0;
                }


                m_turn++;
                checkPromotion();

                int stalemate = checkStalemate((Player)(!tmp->owner()));
                if (stalemate == 1) return 4; //staleMate, gameover

                int checkStatus = checkCheck(end, m_pieces[index(end)], KingPos((Player)(!tmp->owner())));
                if(checkStatus == 1) {
                    return 2; //a special int that indicates the opponent is in check.
                }
                else if (checkStatus == 2) {
                    return 3; //checkMate, gameover
                }

                return 1;
            }
            else if (tmp->id()==5 && abs((int)(end.x-start.x))==2) {
                execute_castle(start,end, tmp);
                return 1;
            }

            else {
                Prompts::illegalMove();
            }
            // int retCode = Board::makeMove(start, end);
        }
        else {
            Prompts::noPiece();
        }
    }
    else {
        Prompts::noPiece();
    }
    return 0;
}

// Setup the chess board with its initial pieces
void ChessGame::setupBoard() {
    std::vector<int> pieces {
        ROOK_ENUM, KNIGHT_ENUM, BISHOP_ENUM, QUEEN_ENUM,
        KING_ENUM, BISHOP_ENUM, KNIGHT_ENUM, ROOK_ENUM
    };
    for (size_t i = 0; i < pieces.size(); ++i) {
        initPiece(PAWN_ENUM, WHITE, Position(i, 1));
        initPiece(pieces[i], WHITE, Position(i, 0));
        initPiece(pieces[i], BLACK, Position(i, 7));
        initPiece(PAWN_ENUM, BLACK, Position(i, 6));
    }
}


///////////////
// CheckValid //
///////////////

int Pawn::validMove(Position start, Position end, const Board& board) const {
    std::vector<Position> moveList;
    if (this->owner() == 0) {
        //if not block in front + not out of board => add 1 move
        if (start.y+1 < 8) {
            if (board.getPiece(Position(start.x, start.y+1)) == nullptr) {
                moveList.push_back(Position(start.x, start.y+1));
            }
            if (start.x > 0 && board.getPiece(Position(start.x-1, start.y+1)) != nullptr && board.getPiece(Position(start.x-1, start.y+1))->owner() == 1) {
                moveList.push_back(Position(start.x-1, start.y+1));
            }
            if (start.x < 7 && board.getPiece(Position(start.x+1, start.y+1)) != nullptr && board.getPiece(Position(start.x+1, start.y+1))->owner() == 1) {
                moveList.push_back(Position(start.x+1, start.y+1));
            }
        }
        if (start.y == 1 && board.getPiece(Position(start.x, start.y+2)) == nullptr) {
            moveList.push_back(Position(start.x, start.y+2));
        }

    }
    else if (this->owner() == 1) {
        if (start.y >= 1) {
            if (board.getPiece(Position(start.x, start.y-1)) == nullptr) {
                moveList.push_back(Position(start.x, start.y-1));
            }
            if (start.x > 0 && board.getPiece(Position(start.x-1, start.y-1)) != nullptr && board.getPiece(Position(start.x-1, start.y-1))->owner() == 0) {
                moveList.push_back(Position(start.x-1, start.y-1));
            }
            if (start.x < 7 && board.getPiece(Position(start.x+1, start.y-1)) != nullptr && board.getPiece(Position(start.x+1, start.y-1))->owner() == 0) {
                moveList.push_back(Position(start.x+1, start.y-1));
            }
        }
        if (start.y == 6 && board.getPiece(Position(start.x, start.y-2)) == nullptr) {
            moveList.push_back(Position(start.x, start.y-2));
        }
    }

    bool valid = false;
    for (std::vector<Position>::iterator i = moveList.begin(); i != moveList.end(); ++i) {
        if (end.x == i->x && end.y == i->y) {
            valid = true;
            return SUCCESS;
        }
    }
    if (!valid) {
        return -1;
    }
    return 0;
}

int Rook::validMove(Position start, Position end, const Board& board) const {
    if ( (start.x!=end.x && start.y==end.y) || (start.x==end.x && start.y!=end.y) ) {
        int diff_x=0, diff_y=0;
        int d1=end.x-start.x;
        int d2=end.y-start.y;
        if ( d1>0 ) {
            diff_x=1;
        }
        else if ( d1<0 ) {
            diff_x=-1;
        }
        if ( d2<0) {
            diff_y=-1;
        }
        else if ( d2>0) {
            diff_y=1;
        }
        int x=start.x, y=start.y;
        while ( x!=((int)end.x) || y!=((int)end.y) ) {
            x+=diff_x;
            y+=diff_y;
            Piece * loc = board.getPiece(Position(x,y));
            if(loc!=nullptr) {
                if( (this->owner()!=loc->owner()) && x==(int)end.x && y==(int)end.y) {
                    return 1;
                }
                // Prompts::blocked();
                return -1;
            }
        }
        return 1;
    }
    else {
        return -1;
    }
}

int Knight::validMove(Position start, Position end, const Board& board) const {
    std::vector<Position> moveList;
    if (start.x >= 2) {
        if (start.y >= 1 && (board.getPiece(Position(start.x-2, start.y-1)) == nullptr || board.getPiece(Position(start.x-2, start.y-1))->owner() != this->owner())) {
            moveList.push_back(Position(start.x-2, start.y-1));
        }
        if (start.y <= 6 && (board.getPiece(Position(start.x-2, start.y+1)) == nullptr || board.getPiece(Position(start.x-2, start.y+1))->owner() != this->owner())) {
            moveList.push_back(Position(start.x-2, start.y+1));
        }
    }
    if (start.x <= 5) {
        if (start.y >= 1 && (board.getPiece(Position(start.x+2, start.y-1)) == nullptr || board.getPiece(Position(start.x+2, start.y-1))->owner() != this->owner())) {
            moveList.push_back(Position(start.x+2, start.y-1));
        }
        if (start.y <= 6 && (board.getPiece(Position(start.x+2, start.y+1)) == nullptr || board.getPiece(Position(start.x+2, start.y+1))->owner() != this->owner())) {
            moveList.push_back(Position(start.x+2, start.y+1));
        }
    }
    if (start.y >= 2) {
        if (start.x >= 1 && (board.getPiece(Position(start.x-1, start.y-2)) == nullptr || board.getPiece(Position(start.x-1, start.y-2))->owner() != this->owner())) {
            moveList.push_back(Position(start.x-1, start.y-2));
        }
        if (start.x <= 6 && (board.getPiece(Position(start.x+1, start.y-2)) == nullptr || board.getPiece(Position(start.x+1, start.y-2))->owner() != this->owner())) {
            moveList.push_back(Position(start.x+1, start.y-2));
        }
    }
    if (start.y <= 5) {
        if (start.x >= 1 && (board.getPiece(Position(start.x-1, start.y+2)) == nullptr || board.getPiece(Position(start.x-1, start.y+2))->owner() != this->owner())) {
            moveList.push_back(Position(start.x-1, start.y+2));
        }
        if (start.x <= 6 && (board.getPiece(Position(start.x+1, start.y+2)) == nullptr || board.getPiece(Position(start.x+1, start.y+2))->owner() != this->owner())) {
            moveList.push_back(Position(start.x+1, start.y+2));
        }
    }
    bool valid = false;
    for (std::vector<Position>::iterator i = moveList.begin(); i != moveList.end(); ++i) {
        if (end.x == i->x && end.y == i->y) {
            valid = true;
            return SUCCESS;
        }
    }
    if (!valid) {
        return -1;
    }
    return 0;
}

int Bishop::validMove(Position start, Position end, const Board& board) const {
    std::vector<Position> moveList;

    int i = 1;
    while(start.x+i < 8 && start.y+i < 8 && (int)start.x+i >=0  && (int)start.y+i >= 0) {
        if (board.getPiece(Position(start.x+i, start.y+i)) == nullptr) {
            moveList.push_back(Position(start.x+i, start.y+i));
        }
        else {
            if (board.getPiece(Position(start.x+i, start.y+i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x+i, start.y+i));
            }
            break;
        }
        i++;
    }

    i = 1;
    while((int)start.x-i >= 0 && start.x-i < 8 && start.y+i < 8 && (int)start.y+i >= 0) {
        if (board.getPiece(Position(start.x-i, start.y+i)) == nullptr) {
            moveList.push_back(Position(start.x-i, start.y+i));
        }
        else {
            if (board.getPiece(Position(start.x-i, start.y+i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x-i, start.y+i));
            }
            break;
        }
        i++;
    }

    i = 1;
    while((int)start.x-i >= 0 && (int)start.y-i >= 0 && start.x-i < 8 && start.y-i < 8) {
        if (board.getPiece(Position(start.x-i, start.y-i)) == nullptr) {
            moveList.push_back(Position(start.x-i, start.y-i));
        }
        else {
            if (board.getPiece(Position(start.x-i, start.y-i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x-i, start.y-i));
            }
            break;
        }
        i++;
    }

    i = 1;
    while(start.x+i < 8 && (int)start.y-i >= 0 && (int)start.x+i >= 0 && start.y-i < 8) {
        if (board.getPiece(Position(start.x+i, start.y-i)) == nullptr) {
            moveList.push_back(Position(start.x+i, start.y-i));
        }
        else {
            if (board.getPiece(Position(start.x+i, start.y-i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x+i, start.y-i));
            }
            break;
        }
        i++;
    }
    bool valid = false;
    for (std::vector<Position>::iterator i = moveList.begin(); i != moveList.end(); ++i) {
        if (end.x == i->x && end.y == i->y) {
            valid = true;
            return 1;
        }
    }
    if (!valid) {
        return -1;
    }
    return 0;
}

int Queen::validMove(Position start, Position end, const Board& board) const {
    std::vector<Position> moveList;

    for (int i = start.x+1; i < 8; i++) {
        if (board.getPiece(Position(i, start.y)) == nullptr) {
            moveList.push_back(Position(i, start.y));
        }
        else {
            if (board.getPiece(Position(i, start.y))->owner() != this->owner()) {
                moveList.push_back(Position(i, start.y));
            }
            break;
        }
    }
    for (int i = start.x-1; i >= 0; i--) {
        if (board.getPiece(Position(i, start.y)) == nullptr) {
            moveList.push_back(Position(i, start.y));
        }
        else {
            if (board.getPiece(Position(i, start.y))->owner() != this->owner()) {
                moveList.push_back(Position(i, start.y));
            }
            break;
        }
    }
    for (int i = start.y+1; i < 8; i++) {
        if (board.getPiece(Position(start.x, i)) == nullptr) {
            moveList.push_back(Position(start.x, i));
        }
        else {
            if (board.getPiece(Position(start.x, i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x, i));
            }
            break;
        }
    }
    for (int i = start.y-1; i >= 0; i--) {
        if (board.getPiece(Position(start.x, i)) == nullptr) {
            moveList.push_back(Position(start.x, i));
        }
        else {
            if (board.getPiece(Position(start.x, i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x, i));
            }
            break;
        }
    }
    int i = 1;
    while(start.x+i < 8 && start.y+i < 8 && (int)start.x+i >= 0 && (int)start.y+i >= 0) {
        if (board.getPiece(Position(start.x+i, start.y+i)) == nullptr) {
            moveList.push_back(Position(start.x+i, start.y+i));
        }
        else {
            if (board.getPiece(Position(start.x+i, start.y+i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x+i, start.y+i));
            }
            break;
        }
        i++;
    }

    i = 1;
    while((int)start.x-i >= 0 && start.x-i < 8 && start.y+i < 8 && (int)start.y+i >= 0) {
        if (board.getPiece(Position(start.x-i, start.y+i)) == nullptr) {
            moveList.push_back(Position(start.x-i, start.y+i));
        }
        else {
            if (board.getPiece(Position(start.x-i, start.y+i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x-i, start.y+i));
            }
            break;
        }
        i++;
    }

    i = 1;
    while((int)start.x-i >= 0 && (int)start.y-i >= 0 && start.x-i < 8 && start.y-i < 8) {
        if (board.getPiece(Position(start.x-i, start.y-i)) == nullptr) {
            moveList.push_back(Position(start.x-i, start.y-i));
        }
        else {
            if (board.getPiece(Position(start.x-i, start.y-i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x-i, start.y-i));
            }
            break;
        }
        i++;
    }

    i = 1;
    while(start.x+i < 8 && (int)start.y-i >= 0 && (int)start.x+i >= 0 && start.y-i < 8) {
        if (board.getPiece(Position(start.x+i, start.y-i)) == nullptr) {
            moveList.push_back(Position(start.x+i, start.y-i));
        }
        else {
            if (board.getPiece(Position(start.x+i, start.y-i))->owner() != this->owner()) {
                moveList.push_back(Position(start.x+i, start.y-i));
            }
            break;
        }
        i++;
    }
    bool valid = false;
    for (std::vector<Position>::iterator i = moveList.begin(); i != moveList.end(); ++i) {
        if (end.x == i->x && end.y == i->y) {
            valid = true;
            return SUCCESS;
        }
    }
    if (!valid) {
        return -1;
    }
    return 0;
}

std::vector<Position> King::getKingMoves(Position start, const Board& board) const {
    std::vector<Position> moveList;
    Piece *king = board.getPiece(Position(start.x, start.y));
    if (start.x >= 1) {
        if (start.y >= 1 && (board.getPiece(Position(start.x-1, start.y-1)) == nullptr || board.getPiece(Position(start.x-1, start.y-1))->owner() != king->owner())) {
            moveList.push_back(Position(start.x-1, start.y-1));
        }
        if (start.y <= 6 && (board.getPiece(Position(start.x-1, start.y+1)) == nullptr || board.getPiece(Position(start.x-1, start.y+1))->owner() != king->owner())) {
            moveList.push_back(Position(start.x-1, start.y+1));
        }
        if ((board.getPiece(Position(start.x-1, start.y)) == nullptr || board.getPiece(Position(start.x-1, start.y))->owner() != king->owner())) {
            moveList.push_back(Position(start.x-1, start.y));
        }
    }
    if (start.x <= 6) {
        if (start.y >= 1 && (board.getPiece(Position(start.x+1, start.y-1)) == nullptr || board.getPiece(Position(start.x+1, start.y-1))->owner() != king->owner())) {
            moveList.push_back(Position(start.x+1, start.y-1));
        }
        if (start.y <= 6 && (board.getPiece(Position(start.x+1, start.y+1)) == nullptr || board.getPiece(Position(start.x+1, start.y+1))->owner() != king->owner())) {
            moveList.push_back(Position(start.x+1, start.y+1));
        }
        if ((board.getPiece(Position(start.x+1, start.y)) == nullptr || board.getPiece(Position(start.x+1, start.y))->owner() != king->owner())) {
            moveList.push_back(Position(start.x+1, start.y));
        }
    }
    if (start.y >= 1) {
        if ((board.getPiece(Position(start.x, start.y-1)) == nullptr || board.getPiece(Position(start.x, start.y-1))->owner() != king->owner())) {
            moveList.push_back(Position(start.x, start.y-1));
        }
    }
    if (start.y <= 6) {
        if ((board.getPiece(Position(start.x, start.y+1)) == nullptr || board.getPiece(Position(start.x, start.y+1))->owner() != king->owner())) {
            moveList.push_back(Position(start.x, start.y+1));
        }
    }
    return moveList;
}

int King::validMove(Position start, Position end, const Board& board) const {
    std::vector<Position> moveList = getKingMoves(start, board);
    bool valid = false;
    for (std::vector<Position>::iterator i = moveList.begin(); i != moveList.end(); ++i) {
        if (end.x == i->x && end.y == i->y) {
            valid = true;
            return SUCCESS;
        }
    }
    if (!valid) {
        return -1;
    }
    return 0;
}