CXX = g++
CXXFLAGS = -Wall -Wextra -pedantic -std=c++11 -g

chess: Chess.o Board.o ChessDriver.o
	$(CXX) $(CXXFLAGS) Chess.o Board.o ChessDriver.o -o chess

unittest: unittest.o Board.o Chess.o
	$(CXX) $(CXXFLAGS) unittest.o Board.o Chess.o -o unittest

Board.o: Board.cpp Game.h
	$(CXX) $(CXXFLAGS) -c Board.cpp

Chess.o: Chess.cpp Game.h Chess.h Prompts.h
	$(CXX) $(CXXFLAGS) -c Chess.cpp

ChessDriver.o: ChessDriver.cpp Game.h Chess.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c ChessDriver.cpp

unittest.o: unittest.cpp Game.h Chess.h Prompts.h Terminal.h
	$(CXX) $(CXXFLAGS) -c unittest.cpp

clean:
	rm -f *.o chess unittest


