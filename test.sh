#!/bin/sh
LAST=4
PASS=true
for ((i=1;i<=$LAST;i++)); do
	./chess in"$i"a.txt $(cat arg"$i"a.txt) > out.txt 
	DIFF=$(diff out.txt out"$i"a.txt)
	if [ "$DIFF" != "" ]; then
		echo "Chess: FAILED test $i!" 
		PASS=false
		break
	fi 
done
if [ $PASS = true ]; then
	echo "Chess: PASSED all tests"
fi
